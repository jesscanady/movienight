Bundler.require
require_relative '../config/plex'

class PlexClient
  URL  = "192.168.1.3"
  PORT = "32400"

  require_relative '../config/plex.rb'

  def initialize
    @server = Plex::Server.new(URL, PORT)
  end

  def movie_titles
    movie_library.all.map(&:title)
  end

  private

  def movie_library
    @server.library.sections.find { |x| x.title == 'Movies'}
  end
end
